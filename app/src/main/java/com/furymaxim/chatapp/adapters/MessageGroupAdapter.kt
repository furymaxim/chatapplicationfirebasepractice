package com.furymaxim.chatapp.adapters

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.helpers.CalendarHelper
import com.furymaxim.chatapp.helpers.PicassoTransform
import com.furymaxim.chatapp.models.Messages
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView


class MessageGroupAdapter(private val mMessageList: MutableList<Messages>, val groupId: String) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mUserDatabase = FirebaseDatabase.getInstance().reference.child("Users")
    private var mMessageDatabase = FirebaseDatabase.getInstance().reference.child("messages")
    private var mChatDatabase = FirebaseDatabase.getInstance().reference.child("Chat")
    private var mAuth = FirebaseAuth.getInstance()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v: View?
        return if (viewType == TYPE_CHAT_USER) {
            v = LayoutInflater.from(parent.context)
                .inflate(R.layout.message_single_layout, parent, false)
            MessageViewHolderChatUser(v)
        } else {
            v = LayoutInflater.from(parent.context)
                .inflate(R.layout.message_single_layout_myself, parent, false)
            MessageViewHolderMyself(v)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (mAuth.currentUser!!.uid == mMessageList[position].from)
            TYPE_MYSELF
        else
            TYPE_CHAT_USER
    }

    inner class MessageViewHolderChatUser(view: View) :
        RecyclerView.ViewHolder(view) {
        var messageText: TextView = view.findViewById<View>(R.id.message_text_layout) as TextView
        var profileImage: CircleImageView =
            view.findViewById<View>(R.id.message_profile_layout) as CircleImageView
        var displayName: TextView = view.findViewById<View>(R.id.name_text_layout) as TextView
        var messageImage: ImageView =
            view.findViewById<View>(R.id.message_image_layout) as ImageView
        var messageTime: TextView = view.findViewById(R.id.time_text_layout) as TextView
    }

    inner class MessageViewHolderMyself(view: View) :
        RecyclerView.ViewHolder(view) {
        var messageText: TextView = view.findViewById<View>(R.id.message_text_layout) as TextView
        var messageImage: ImageView =
            view.findViewById<View>(R.id.message_image_layout) as ImageView
        var messageTime: TextView = view.findViewById(R.id.time_text_layout) as TextView
        var deleteBtn: ImageView = view.findViewById(R.id.deleteMessage) as ImageView
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        mAuth = FirebaseAuth.getInstance()
        val c = mMessageList[i]
        val fromUser = c.from
        val messageType = c.type
        var seen = false
        val timePure = c.time
        val messageView = c.view
        val time = CalendarHelper.convertMilliSecondsToHourMins(c.time.toString())

        if (fromUser != mAuth.currentUser!!.uid) {
            seen = true
        } else {
            mChatDatabase.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(p0: DataSnapshot) {
                    seen = false

                    for (user in p0.children) {
                        if (user.value.toString() != mAuth.currentUser!!.uid) {
                            if (user.child("groups").child(groupId).child("seen ").value == true) {
                                seen = true
                            }
                        }
                    }
                }
            })
        }


        if (getItemViewType(i) == TYPE_CHAT_USER) {
            mUserDatabase = FirebaseDatabase.getInstance().reference.child("Users").child(fromUser)
            mUserDatabase.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val name = dataSnapshot.child("name").value.toString()
                    val image = dataSnapshot.child("image").value.toString()

                    (viewHolder as MessageViewHolderChatUser).displayName.text = name

                    Picasso.get().load(image)
                        .placeholder(R.drawable.default_avatar)
                        .into((viewHolder).profileImage)

                }

                override fun onCancelled(databaseError: DatabaseError) {}
            })

            (viewHolder as MessageViewHolderChatUser).messageTime.text = time

            if (messageView != "deleted") {
                if (messageType == "text") {
                    viewHolder.messageText.text = c.message
                    if (seen) {
                        viewHolder.messageText.setTypeface(
                            viewHolder.messageText.typeface,
                            Typeface.BOLD
                        )
                    }
                    viewHolder.messageImage.visibility = View.INVISIBLE
                } else {
                    viewHolder.messageText.visibility = View.INVISIBLE
                    Picasso.get().load(c.message)
                        .transform(PicassoTransform())
                        .placeholder(R.drawable.default_avatar)
                        .into(viewHolder.messageImage)
                }
            } else {
                viewHolder.messageText.text = "Сообщение удалено"

                if (seen) {
                    viewHolder.messageText.setTypeface(
                        viewHolder.messageText.typeface,
                        Typeface.BOLD
                    )
                }
            }
            viewHolder.itemView.setOnClickListener {
            }
        } else {
            (viewHolder as MessageViewHolderMyself).messageTime.text = time

            if (messageView != "deleted") {
                viewHolder.deleteBtn.visibility = View.VISIBLE
                if (messageType == "text") {
                    viewHolder.messageText.text = c.message
                    if (seen) {
                        viewHolder.messageText.setTypeface(
                            viewHolder.messageText.typeface,
                            Typeface.BOLD
                        )
                    }
                    viewHolder.messageImage.visibility = View.INVISIBLE
                } else {
                    viewHolder.messageText.visibility = View.INVISIBLE

                    Picasso.get().load(c.message)
                        .transform(PicassoTransform())
                        .placeholder(R.drawable.default_avatar)
                        .into(viewHolder.messageImage)
                }
            } else {
                viewHolder.messageText.text = "Сообщение удалено"
                viewHolder.deleteBtn.visibility = View.GONE
            }


            viewHolder.deleteBtn.setOnClickListener {
                mMessageDatabase.child(groupId).addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        for (snapshot in p0.children) {
                            val pushId = snapshot.key.toString()
                            val message = snapshot.getValue(Messages::class.java)
                            if (message!!.from == mAuth.currentUser!!.uid && message.time == timePure) {
                                mMessageDatabase
                                    .child(groupId)
                                    .child(pushId)
                                    .child("view")
                                    .setValue("deleted")
                                    .addOnSuccessListener {
                                        mMessageDatabase
                                            .child(groupId)
                                            .child(pushId)
                                            .child("view")
                                            .setValue("deleted")
                                            .addOnSuccessListener {
                                                viewHolder.messageText.text =
                                                    "Cообщение удалено"
                                                viewHolder.deleteBtn.visibility =
                                                    View.GONE
                                            }
                                    }


                            }
                        }

                    }
                })
            }
        }

    }

    override fun getItemCount(): Int {
        return mMessageList.size
    }

    companion object {
        private const val TYPE_MYSELF = 1
        private const val TYPE_CHAT_USER = 2
    }

}
