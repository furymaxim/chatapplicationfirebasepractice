package com.furymaxim.chatapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.models.User
import com.furymaxim.chatapp.utils.OnItemCheckListener
import com.squareup.picasso.Picasso


class GroupChatRequestAdapter(private val mUsersList: MutableList<User>, private var onItemCheckListener: OnItemCheckListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return GroupChatRequestViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.group_friend_layout, parent, false))
    }

    class GroupChatRequestViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var image: ImageView = view.findViewById(R.id.thumbnail)
        var name: TextView = view.findViewById(R.id.username)
        var checkbox: CheckBox = view.findViewById(R.id.checkbox)

        init{
            checkbox.isClickable = false
        }

        fun setOnClickListener(clickListener: View.OnClickListener){
            itemView.setOnClickListener(clickListener)
        }

    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        val userName = mUsersList[i].name
        val userImage = mUsersList[i].image

        Picasso.get().load(userImage)
            .placeholder(R.drawable.default_avatar)
            .into((viewHolder as GroupChatRequestViewHolder).image)

        (viewHolder).name.text = userName

        viewHolder.setOnClickListener(View.OnClickListener {
            viewHolder.checkbox.isChecked = !viewHolder.checkbox.isChecked
            if(viewHolder.checkbox.isChecked){
                onItemCheckListener.onItemCheck(mUsersList[i])
            }else{
                onItemCheckListener.onItemUncheck(mUsersList[i])
            }
        })

    }

    override fun getItemCount(): Int {
        return mUsersList.size
    }




}
