package com.furymaxim.chatapp.ui.fragments.main_group.tabs


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.models.Request
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_requests_tab.*
import kotlinx.android.synthetic.main.request_layout.view.*

class RequestsTabFragment : Fragment() {


    private var mUserDatabaseRef: DatabaseReference? = null
    private var mFriendReqDatabaseRef: DatabaseReference? = null
    private var mFriendDatabaseRef: DatabaseReference? = null
    private var mRootDatabaseRef: DatabaseReference? = null
    private var mCurrentUser: FirebaseUser? = null
    private var currentUserId: String? = null
    private var adapter: MyFirebaseRecyclerAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_requests_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mCurrentUser = FirebaseAuth.getInstance().currentUser
        currentUserId = mCurrentUser!!.uid
        mUserDatabaseRef = FirebaseDatabase.getInstance().reference.child("Users")
        mRootDatabaseRef =
            FirebaseDatabase.getInstance().reference.child("Friend_req").child(currentUserId!!)
        mFriendReqDatabaseRef = FirebaseDatabase.getInstance().reference.child("Friend_req")
        mFriendDatabaseRef = FirebaseDatabase.getInstance().reference.child("Friends")

        val linearLayoutManager = LinearLayoutManager(context)

        mRequestsList.layoutManager = linearLayoutManager
    }

    override fun onStart() {
        super.onStart()

        val options: FirebaseRecyclerOptions<Request> = FirebaseRecyclerOptions.Builder<Request>()
            .setQuery(mRootDatabaseRef!!, Request::class.java)
            .build()

        adapter = MyFirebaseRecyclerAdapter(options)

        mRequestsList.adapter = adapter

        adapter!!.startListening()
    }

    inner class MyFirebaseRecyclerAdapter internal constructor(options: FirebaseRecyclerOptions<Request>) :
        FirebaseRecyclerAdapter<Request, RequestViewHolder>(options) {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RequestViewHolder {
            return RequestViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.request_layout,
                    parent,
                    false
                )
            )
        }

        override fun onBindViewHolder(holder: RequestViewHolder, position: Int, model: Request) {

            val listUserId = getRef(position).key
            if (mRootDatabaseRef != null) {
                mRootDatabaseRef!!.child(listUserId!!)
                    .addValueEventListener(object : ValueEventListener {

                        override fun onCancelled(p0: DatabaseError) {
                        }

                        override fun onDataChange(p0: DataSnapshot) {
                            if (p0.exists()) {
                                if (p0.child("request_type").value.toString() == "received") {
                                    mUserDatabaseRef!!.child(listUserId)
                                        .addValueEventListener(object : ValueEventListener {
                                            override fun onCancelled(p0: DatabaseError) {
                                            }

                                            override fun onDataChange(p0: DataSnapshot) {
                                                val userName = p0.child("name").value.toString()
                                                val userImage: String =
                                                    p0.child("image").value.toString()
                                                val currentDate = ServerValue.TIMESTAMP

                                                holder.setName(userName)
                                                holder.setImage(userImage)
                                                holder.view.acceptBtn.setOnClickListener {
                                                    mFriendDatabaseRef!!.child(mCurrentUser!!.uid)
                                                        .child(listUserId)
                                                        .child("date")
                                                        .setValue(currentDate)
                                                        .addOnSuccessListener {
                                                            mFriendDatabaseRef!!.child(listUserId)
                                                                .child(mCurrentUser!!.uid)
                                                                .child("date")
                                                                .setValue(currentDate)
                                                                .addOnSuccessListener {
                                                                    mFriendReqDatabaseRef!!.child(
                                                                        mCurrentUser!!.uid
                                                                    ).child(listUserId)
                                                                        .removeValue()
                                                                        .addOnSuccessListener {
                                                                            mFriendReqDatabaseRef!!.child(
                                                                                listUserId
                                                                            )
                                                                                .child(mCurrentUser!!.uid)
                                                                                .removeValue()
                                                                        }
                                                                }
                                                        }
                                                }
                                                holder.view.rejectBtn.setOnClickListener {
                                                    mFriendReqDatabaseRef!!.child(mCurrentUser!!.uid)
                                                        .child(listUserId).removeValue()
                                                    mFriendReqDatabaseRef!!.child(listUserId)
                                                        .child(mCurrentUser!!.uid).removeValue()
                                                }
                                            }
                                        })
                                }else{
                                    holder.view.visibility = View.GONE
                                }
                            }
/*                            else{
                                holder.view.visibility = View.GONE
                            }*/
                        }
                    })
            }
            /*else {
                holder.view.visibility = View.GONE
            }*/


        }
    }

    inner class RequestViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        private var image: ImageView? = null
        private var name: TextView? = null

        init {
            image = view.findViewById(R.id.thumbnail)
            name = view.findViewById(R.id.username)

        }

        fun setName(username: String) {
            name!!.text = username
        }

        fun setImage(imageLink: String) {
            Picasso.get().load(imageLink).placeholder(R.drawable.default_avatar).into(image!!)
        }


    }

    override fun onStop() {
        super.onStop()

        adapter!!.stopListening()
    }


}


