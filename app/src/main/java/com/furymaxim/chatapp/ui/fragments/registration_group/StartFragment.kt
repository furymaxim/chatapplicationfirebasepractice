package com.furymaxim.chatapp.ui.fragments.registration_group


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.ui.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_start.*

class StartFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_start, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        startRegBtn.setOnClickListener {
            replaceFragment(R.id.fragmentContainer, RegistrationFragment(),null,null)
        }

        signInBtn.setOnClickListener {
            replaceFragment(R.id.fragmentContainer, LoginFragment(),null,null)
        }

    }



}
