package com.furymaxim.chatapp.ui.fragments.main_group


import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.ui.activities.MainActivity
import com.furymaxim.chatapp.ui.fragments.BaseFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_profile.*


class ProfileFragment : BaseFragment() {

    private var mCurrentState: String? = null
    private val mAuth = FirebaseAuth.getInstance()
    private val mUsersDatabaseRef = FirebaseDatabase.getInstance().reference.child("Users")
    private val mFriendReqDatabaseRef = FirebaseDatabase.getInstance().reference.child("Friend_req")
    private val mFriendDatabaseRef = FirebaseDatabase.getInstance().reference.child("Friends")
    private val mMyFriendDatabaseRef = FirebaseDatabase.getInstance().reference.child("Friends")
    private val mNotificationDatabaseRef = FirebaseDatabase.getInstance().reference.child("Notifications")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val userId = arguments!!.getString("uid")

        (activity as MainActivity).supportActionBar!!.hide()

        mCurrentState = if(savedInstanceState == null){
            "not_friends"
        }else{
            savedInstanceState.getString("state")
        }


        mMyFriendDatabaseRef.child(userId!!).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                if(p0.exists()){
                    val count = p0.childrenCount
                    countOfFriends.text = count.toString()
                }else{
                    countOfFriends.text = "0"
                }
            }
        })

        mUsersDatabaseRef.child(userId).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {

                val name = p0.child("name").value.toString()
                val status = p0.child("status").value.toString()
                val avatar = p0.child("image").value.toString()

                if (context != null) {
                    if (avatar != "default") {
                        Glide
                            .with(context!!)
                            .load(avatar)
                            .placeholder(R.drawable.default_avatar)
                            .into(profileImage)
                    } else {
                        profileImage.setImageDrawable(
                            resources.getDrawable(
                                R.drawable.default_avatar,
                                null
                            )
                        )
                    }
                }

                if (mAuth.currentUser!!.uid == userId) {
                    if (btnSendFriendRequest != null) btnSendFriendRequest.visibility = View.GONE
                    if (btnSendFriendRequest != null) btnDeclineFriendRequest.visibility = View.GONE
                    if (layoutFriendStats != null) layoutFriendStats.visibility = View.GONE

                } else {
                    mFriendReqDatabaseRef.child(mAuth.currentUser!!.uid)
                        .addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onDataChange(p0: DataSnapshot) {
                                if (p0.hasChild(userId)) {
                                    val reqType =
                                        p0.child(userId).child("request_type").value.toString()
                                    if (reqType == "received") {
                                        mCurrentState = "req_received"
                                        btnSendFriendRequest.text = "Принять запрос дружбы"
                                        btnDeclineFriendRequest.visibility = View.VISIBLE
                                        btnDeclineFriendRequest.setOnClickListener {
                                            mFriendReqDatabaseRef.child(mAuth.currentUser!!.uid)
                                                .child(userId).removeValue()
                                            mFriendReqDatabaseRef.child(userId)
                                                .child(mAuth.currentUser!!.uid).removeValue()
                                                .addOnSuccessListener {
                                                    mCurrentState = "not_friends"
                                                    btnDeclineFriendRequest.visibility = View.GONE
                                                }

                                        }
                                    } else {
                                        if (reqType == "sent") {
                                            mCurrentState = "req_sent"
                                            btnSendFriendRequest.text = "Отменить запрос"
                                        }
                                    }
                                } else {
                                    mFriendDatabaseRef.child(mAuth.currentUser!!.uid)
                                        .addListenerForSingleValueEvent(object :
                                            ValueEventListener {
                                            override fun onDataChange(p0: DataSnapshot) {
                                                if (p0.hasChild(userId)) {
                                                    mCurrentState = "friends"
                                                    btnSendFriendRequest.text = "Удалить из друзей"
                                                }
                                            }

                                            override fun onCancelled(p0: DatabaseError) {
                                            }
                                        })
                                }
                            }

                            override fun onCancelled(p0: DatabaseError) {
                            }
                        })
                }

                Handler().postDelayed({

                    if (profileDisplayName != null) profileDisplayName.text = name
                    if (profileStatus != null) profileStatus.text = status
                    if (profileImage != null) {
                        if (profileImage.visibility == View.INVISIBLE) {
                            progressLayout.visibility = View.GONE
                            profileImage.visibility = View.VISIBLE
                        }
                    }
                }, 200)
            }
        })

        btnSendFriendRequest.setOnClickListener {

            btnSendFriendRequest.isEnabled = false
            btnSendFriendRequest.setBackgroundColor(
                resources.getColor(
                    R.color.btnDisabledColor,
                    null
                )
            )

            if (mCurrentState == "not_friends") {

                mFriendReqDatabaseRef.child(mAuth.currentUser!!.uid).child(userId)
                    .child("request_type").setValue("sent")
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            mFriendReqDatabaseRef.child(userId).child(mAuth.currentUser!!.uid)
                                .child("request_type").setValue("received")
                                .addOnSuccessListener {
                                    val notificationData = HashMap<String, String>()
                                    notificationData["from"] = mAuth.currentUser!!.uid
                                    notificationData["type"] = "request"


                                    btnSendFriendRequest.isEnabled = true
                                    mNotificationDatabaseRef.child("userId").push()
                                        .setValue(notificationData)
                                        .addOnSuccessListener {

                                            mCurrentState = "req_sent"
                                            btnSendFriendRequest.text = "Отменить запрос"
                                            btnSendFriendRequest.setBackgroundColor(
                                                resources.getColor(
                                                    R.color.colorAccent,
                                                    null
                                                )
                                            )
                                            Toast.makeText(
                                                context,
                                                "Запрос успешно отправлен",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        }

                                }
                        } else {
                            Toast.makeText(
                                context,
                                "Отправка запроса не удалась",
                                Toast.LENGTH_SHORT
                            ).show()
                            btnSendFriendRequest.isEnabled = true
                            btnSendFriendRequest.setBackgroundColor(
                                resources.getColor(
                                    R.color.colorAccent,
                                    null
                                )
                            )
                        }
                    }
            }

            // ---CANCEL REQUEST STATE--------
            if (mCurrentState == "req_sent") {
                mFriendReqDatabaseRef.child(mAuth.currentUser!!.uid).child(userId)
                    .removeValue()
                    .addOnSuccessListener {
                        mFriendReqDatabaseRef.child(userId).child(mAuth.currentUser!!.uid)
                            .removeValue()
                            .addOnSuccessListener {
                                btnSendFriendRequest.setBackgroundColor(
                                    resources.getColor(
                                        R.color.colorAccent,
                                        null
                                    )
                                )
                                btnSendFriendRequest.isEnabled = true
                                mCurrentState = "not_friends"
                                btnSendFriendRequest.text = "Предложить дружбу"
                            }
                    }
                    .addOnFailureListener {
                        btnSendFriendRequest.setBackgroundColor(
                            resources.getColor(
                                R.color.btnDisabledColor,
                                null
                            )
                        )
                        btnSendFriendRequest.isEnabled = false
                    }
            }


            // ---- RECEIVED STATE---
            if (mCurrentState == "req_received") {
                val currentDate = ServerValue.TIMESTAMP

                mFriendDatabaseRef.child(mAuth.currentUser!!.uid)
                    .child(userId)
                    .child("date")
                    .setValue(currentDate)
                    .addOnSuccessListener {
                        mFriendDatabaseRef.child(userId)
                            .child(mAuth.currentUser!!.uid)
                            .child("date")
                            .setValue(currentDate)
                            .addOnSuccessListener {
                                mFriendReqDatabaseRef.child(mAuth.currentUser!!.uid).child(userId)
                                    .removeValue()
                                    .addOnSuccessListener {
                                        mFriendReqDatabaseRef.child(userId)
                                            .child(mAuth.currentUser!!.uid)
                                            .removeValue()
                                            .addOnSuccessListener {
                                                btnSendFriendRequest.setBackgroundColor(
                                                    resources.getColor(
                                                        R.color.colorAccent,
                                                        null
                                                    )
                                                )
                                                btnSendFriendRequest.isEnabled = true
                                                btnDeclineFriendRequest.visibility = View.GONE
                                                mCurrentState = "friends"
                                                btnSendFriendRequest.text = "Удалить из друзей"

                                            }
                                    }
                                    .addOnFailureListener {
                                        btnSendFriendRequest.setBackgroundColor(
                                            resources.getColor(
                                                R.color.btnDisabledColor,
                                                null
                                            )
                                        )
                                        btnSendFriendRequest.isEnabled = false
                                    }
                            }
                    }

            }

            if (mCurrentState == "friends") {
                mFriendDatabaseRef
                    .child(mAuth.currentUser!!.uid)
                    .child(userId)
                    .removeValue()
                    .addOnSuccessListener {
                        mFriendDatabaseRef
                            .child(userId)
                            .child(mAuth.currentUser!!.uid)
                            .removeValue()
                        Toast.makeText(context, "Пользователь удален из друзей", Toast.LENGTH_SHORT)
                            .show()
                        btnSendFriendRequest.setBackgroundColor(
                            resources.getColor(
                                R.color.colorAccent,
                                null
                            )
                        )
                        btnSendFriendRequest.isEnabled = true
                        mCurrentState = "not_friends"
                        btnSendFriendRequest.text = "Предложить дружбу"
                    }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString("state", mCurrentState)
    }

    companion object {
        fun newInstance(uid: String): ProfileFragment {
            val fragment = ProfileFragment()
            val args = Bundle()

            args.putString("uid", uid)
            fragment.arguments = args

            return fragment
        }
    }


}
