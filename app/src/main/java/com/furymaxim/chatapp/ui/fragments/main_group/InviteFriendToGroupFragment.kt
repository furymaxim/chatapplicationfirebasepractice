package com.furymaxim.chatapp.ui.fragments.main_group


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.adapters.GroupChatRequestAdapter
import com.furymaxim.chatapp.models.User
import com.furymaxim.chatapp.ui.fragments.BaseFragment
import com.furymaxim.chatapp.utils.OnItemCheckListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_invite_friend_to_group_fragment.*
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.MutableList
import kotlin.collections.MutableMap
import kotlin.collections.set

class InviteFriendToGroupFragment : BaseFragment(), OnItemCheckListener {


    private val mAuth = FirebaseAuth.getInstance()
    private val mCurrentUserId = mAuth.currentUser!!.uid
    private val mFriendDatabase =
        FirebaseDatabase.getInstance().reference.child("Friends").child(mCurrentUserId)
    private val mUsersDatabase = FirebaseDatabase.getInstance().reference.child("Users")
    private val mGroupReqDatabase = FirebaseDatabase.getInstance().reference.child("Groups_invites")
    private val mChatDatabase =
        FirebaseDatabase.getInstance().reference
            .child("Chat")

    private var checkedFriendsList: ArrayList<User> = ArrayList()

    private var itemPos = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_invite_friend_to_group_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val groupName = arguments!!.getString("groupName")
        val creator = arguments!!.getString("creator")
        val mGroupId = arguments!!.getString("groupId")

        val friendsList: MutableList<User> = ArrayList()

        val mAdapter = GroupChatRequestAdapter(friendsList, this)

        if (savedInstanceState != null) {
            checkedFriendsList = savedInstanceState.getParcelableArrayList("list")!!
        }

        usersList.layoutManager = LinearLayoutManager(context)

        usersList.adapter = mAdapter

        loadUsersToInvite(friendsList)

        inviteUsers.setOnClickListener {
            if (checkedFriendsList.size > 0) {
                for (friend in checkedFriendsList) {
                    mUsersDatabase.addValueEventListener(object : ValueEventListener {
                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            for (item in dataSnapshot.children) {
                                if (item!!.child("name").value == friend.name) {
                                    val groupChatMap =
                                        HashMap<String, Any>() as MutableMap<String, Any>
                                    groupChatMap["seen"] = false
                                    groupChatMap["timestamp"] = 0
                                    mChatDatabase.child(item.key!!.toString())
                                        .child("groups").child(mGroupId!!)
                                        .setValue(groupChatMap)
                                    mGroupReqDatabase.child(item.key.toString())
                                        .child("state").setValue("active")
                                }
                            }

                        }

                        override fun onCancelled(p0: DatabaseError) {

                        }
                    })

                    replaceFragment(
                        R.id.fragmentContainer,
                        GroupsFragment.newInstance(),
                        null,
                        null
                    )

                }
            } else {
                Toast.makeText(context!!, "Некого приглашать", Toast.LENGTH_SHORT).show()
            }
        }

        back.setOnClickListener {
            replaceFragment(
                R.id.fragmentContainer,
                GroupConvFragment.newInstance(mGroupId!!, groupName!!, creator!!),
                null,
                null
            )
        }

    }


    private fun loadUsersToInvite(friendsList: MutableList<User>) {
        val mGroupId = arguments!!.getString("groupId")

        mFriendDatabase.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {

            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {

            }

            override fun onChildAdded(dataSnapshot: DataSnapshot, p1: String?) {
                val friendId = dataSnapshot.key.toString()

                mChatDatabase.child(friendId).child("groups")
                    .addValueEventListener(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {

                        }

                        override fun onDataChange(p0: DataSnapshot) {
                            if (!p0.hasChild(mGroupId!!)) {
                                mUsersDatabase.child(friendId)
                                    .addValueEventListener(object : ValueEventListener {
                                        override fun onCancelled(p0: DatabaseError) {

                                        }

                                        override fun onDataChange(p0: DataSnapshot) {
                                            val userName = p0.child("name").value.toString()
                                            val userImage = p0.child("image").value.toString()
                                            val status = p0.child("status").value.toString()

                                            friendsList.add(
                                                itemPos++,
                                                User(userName, status, userImage)
                                            )

                                            (usersList.adapter as GroupChatRequestAdapter).notifyDataSetChanged()
                                        }
                                    })
                            }

                        }
                    })

            }

            override fun onChildRemoved(p0: DataSnapshot) {

            }
        })
    }


    override fun onItemCheck(item: User) {
        checkedFriendsList.add(item)
    }

    override fun onItemUncheck(item: User) {
        checkedFriendsList.remove(item)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList("list", checkedFriendsList)
    }


    companion object {
        fun newInstance(
            groupId: String,
            groupName: String,
            creator: String
        ): InviteFriendToGroupFragment {
            val fragment =
                InviteFriendToGroupFragment()
            val args = Bundle()

            args.putString("groupId", groupId)
            args.putString("groupName", groupName)
            args.putString("creator", creator)
            fragment.arguments = args

            return fragment
        }
    }
}
