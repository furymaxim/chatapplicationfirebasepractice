package com.furymaxim.chatapp.ui.fragments.main_group.tabs


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions

import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.helpers.CalendarHelper
import com.furymaxim.chatapp.models.Friends
import com.furymaxim.chatapp.models.User
import com.furymaxim.chatapp.ui.fragments.BaseFragment
import com.furymaxim.chatapp.ui.fragments.main_group.ConversationFragment
import com.furymaxim.chatapp.ui.fragments.main_group.ProfileFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.dialog_choose.view.*
import kotlinx.android.synthetic.main.fragment_friends_tab.*

class FriendsTabFragment : BaseFragment() {

    private var mFriendsDatabaseRef: DatabaseReference? = null
    private var mUsersDatabaseRef: DatabaseReference? = null
    private var mFireBaseAuth: FirebaseAuth? = null
    private var adapter: MyFirebaseRecyclerAdapter? = null
    private var currentUser: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_friends_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mFireBaseAuth = FirebaseAuth.getInstance()
        currentUser = mFireBaseAuth!!.currentUser!!.uid

        mFriendsDatabaseRef =
            FirebaseDatabase.getInstance().reference.child("Friends").child(currentUser!!)
        mFriendsDatabaseRef!!.keepSynced(true)
        mUsersDatabaseRef = FirebaseDatabase.getInstance().reference.child("Users")
        mUsersDatabaseRef!!.keepSynced(true)

        recyclerView.layoutManager = LinearLayoutManager(context)
    }


    override fun onStart() {
        super.onStart()

        val options: FirebaseRecyclerOptions<Friends> = FirebaseRecyclerOptions.Builder<Friends>()
            .setQuery(mFriendsDatabaseRef!!, Friends::class.java)
            .build()

        adapter = MyFirebaseRecyclerAdapter(options)

        recyclerView.adapter = adapter

        adapter!!.startListening()
    }

    override fun onStop() {
        super.onStop()

        adapter!!.stopListening()
    }

    inner class MyFirebaseRecyclerAdapter internal constructor(options: FirebaseRecyclerOptions<Friends>) :
        FirebaseRecyclerAdapter<Friends, FriendsViewHolder>(options) {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FriendsViewHolder {
            return FriendsViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.single_user_layout,
                    parent,
                    false
                )
            )
        }

        override fun onBindViewHolder(holder: FriendsViewHolder, position: Int, model: Friends) {
            val formattedDate = CalendarHelper.convertMilliSecondsToMonth(model.date.toString())
            holder.setStatus("Дружите с $formattedDate")

            val listUserId = getRef(position).key

            mUsersDatabaseRef!!.child(listUserId!!)
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(p0: DataSnapshot) {
                        val userName = p0.child("name").value.toString()
                        val userImage = p0.child("image").value.toString()

                   if (p0.hasChild("online")) {
                            val userOnline = p0.child("online").value.toString()
                            holder.setUserOnline(userOnline)
                        }

                        holder.setName(userName)
                        holder.setImage(userImage)

                        holder.view.setOnClickListener {
                            val mDialogView =
                                LayoutInflater.from(context).inflate(R.layout.dialog_choose, null)

                            val mBuilder = AlertDialog.Builder(context!!, R.style.CustomDialog)
                                .setView(mDialogView)

                            val mAlertDialog = mBuilder.create()

                            mAlertDialog.show()

                            mAlertDialog.setCanceledOnTouchOutside(true)
                            mAlertDialog.setCancelable(true)

                            mDialogView.openProfile.setOnClickListener {
                                val fragmentManager = activity!!.supportFragmentManager
                                fragmentManager.beginTransaction()
                                    .replace(R.id.fragmentContainer, ProfileFragment.newInstance(listUserId))
                                    .addToBackStack(null)
                                    .commit()

                                mAlertDialog.hide()
                            }

                            mDialogView.sendMessage.setOnClickListener {
                                val fragmentManager = activity!!.supportFragmentManager
                                fragmentManager.beginTransaction()
                                    .replace(R.id.fragmentContainer, ConversationFragment.newInstance(listUserId, userName))
                                    .addToBackStack(null)
                                    .commit()

                                mAlertDialog.hide()
                            }



                        }
                    }
                })
        }
    }

    inner class FriendsViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        private var image: ImageView? = null
        private var status: TextView? = null
        private var name: TextView? = null
        private var userOnline: ImageView? = null

        init {
            image = view.findViewById(R.id.thumbnail)
            status = view.findViewById(R.id.status)
            name = view.findViewById(R.id.username)
            userOnline = view.findViewById(R.id.userOnline)
        }

        fun setName(username: String) {
            name!!.text = username
        }

        fun setStatus(userStatus: String) {
            status!!.text = userStatus
        }

        fun setImage(imageLink: String) {
            if (imageLink != "default") {
                Glide
                    .with(context!!)
                    .load(imageLink)
                    .into(image!!)
            }
        }

        fun setUserOnline(status: String) {
            if (status == "true") {
                userOnline!!.visibility = View.VISIBLE
            } else {
                userOnline!!.visibility = View.INVISIBLE
            }
        }
    }


}
