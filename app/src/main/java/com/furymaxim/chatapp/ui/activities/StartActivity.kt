package com.furymaxim.chatapp.ui.activities

import android.os.Bundle
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.ui.fragments.registration_group.StartFragment

class StartActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        replaceFragment(
            R.id.fragmentContainer,
            StartFragment(), null, null
        )
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            if (supportFragmentManager.backStackEntryCount == 1) {
                finish()
            } else
                supportFragmentManager.popBackStack()
        } else
            finish()
    }
}
