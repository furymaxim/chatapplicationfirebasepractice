package com.furymaxim.chatapp.ui.fragments.registration_group


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.ui.fragments.BaseFragment
import com.furymaxim.chatapp.ui.activities.MainActivity
import com.furymaxim.chatapp.ui.activities.StartActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.progressLayout
import kotlinx.android.synthetic.main.fragment_registration.back


class LoginFragment : BaseFragment() {

    private val mAuth = FirebaseAuth.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mSharedPreferences = context!!.getSharedPreferences("loginPref", Context.MODE_PRIVATE)

        if(mSharedPreferences!!.contains("email")) loginEmail.setText(mSharedPreferences.getString("email",""))

        back.setOnClickListener {
            replaceFragment(R.id.fragmentContainer, StartFragment(), null, null)
        }

        loginBtn.setOnClickListener {
            if (loginEmail.text.isNotEmpty() && loginPassword.text.isNotEmpty()) {
                val inputManager: InputMethodManager =
                    context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(
                    loginBtn.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )

                progressLayout.visibility = View.VISIBLE

                Handler().postDelayed({
                    loginUser(loginEmail.text.toString(), loginPassword.text.toString())
                }, 1000)
            }

        }
    }

    private fun loginUser(email: String, password: String) {
        val mSharedPreferences = context!!.getSharedPreferences("loginPref", Context.MODE_PRIVATE)

        mAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(activity as StartActivity
            ) {
                if (it.isSuccessful) {
                    Log.d(TAG, "signInWithEmail:success")
                    progressLayout.visibility = View.GONE

                    mSharedPreferences!!.edit().putString("email",email).apply()

                    val mainIntent = Intent(
                        activity as StartActivity,
                        MainActivity::class.java
                    )

                    mainIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

                    startActivity(mainIntent)

                    (activity as StartActivity).finish()
                } else {
                    Log.w(TAG, "signInWithEmail:failure", it.exception)
                    progressLayout.visibility = View.GONE
                    Toast.makeText(
                        context,
                        "Не могу войти. Проверь введенный данные или попробуй позднее. Reason: ${it.exception.toString()}",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }

    companion object {
        private const val TAG = "login"
    }
}
