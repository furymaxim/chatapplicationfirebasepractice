package com.furymaxim.chatapp.ui.fragments.main_group


import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.adapters.MessageAdapter
import com.furymaxim.chatapp.helpers.GetTimeAgo
import com.furymaxim.chatapp.models.Messages
import com.furymaxim.chatapp.ui.activities.MainActivity
import com.furymaxim.chatapp.ui.fragments.BaseFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.fragment_conversation.*
import kotlinx.android.synthetic.main.fragment_settings.back


class ConversationFragment : BaseFragment() {
    private var mChatUser: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_conversation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mRootRef = FirebaseDatabase.getInstance().reference
        val mCurrentUserId = FirebaseAuth.getInstance().currentUser!!.uid
        val messagesList: MutableList<Messages> = ArrayList()
        val userName = arguments!!.getString("username")
        val dialog = SpotsDialog.Builder()
            .setContext(context)
            .setCancelable(false)
            .setTheme(R.style.ProgressDialog)
            .build()

        (activity as MainActivity).supportActionBar!!.hide()

        mChatUser = if(savedInstanceState == null) {
            arguments!!.getString("uid")
        }else{
            savedInstanceState.getString("chatUser")
        }

        mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser!!).child("seen").setValue(true)

        dialog.show()

        val mAdapter = MessageAdapter(messagesList, mChatUser!!)

        mMessagesList.layoutManager = LinearLayoutManager(context)
        mMessagesList.adapter = mAdapter

        Handler(Looper.getMainLooper()).postDelayed({
            userNameTV.text = userName

            loadMessages(messagesList)

            mRootRef.child("Users").child(mChatUser!!)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val online = dataSnapshot.child("online").value.toString()
                        val image = dataSnapshot.child("image").value.toString()
                        if (online == "true") {
                            if (mLastSeenView != null) {
                                mLastSeenView.text = "Онлайн"
                            }
                        } else {
                            val lastTime = online.toLong()
                            val lastSeenTime: String? = GetTimeAgo.getTimeAgo(lastTime, context!!)
                            if (lastSeenTime != null && mLastSeenView != null) {
                                mLastSeenView.text = lastSeenTime
                            }
                        }
                        if (imageView != null) {
                            Picasso.get().load(image).placeholder(R.drawable.default_avatar)
                                .into(imageView)
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })


            mRootRef.child("Chat").child(mCurrentUserId)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        if (!dataSnapshot.hasChild(mChatUser!!)) {
                            val chatAddMap = HashMap<String, Any>() as MutableMap<String, Any>

                            chatAddMap["seen"] = false
                            chatAddMap["timestamp"] = ServerValue.TIMESTAMP

                            val chatUserMap = HashMap<String, Any>() as MutableMap<String, Any>

                            chatUserMap["Chat/$mCurrentUserId/$mChatUser"] = chatAddMap
                            chatUserMap["Chat/$mChatUser/$mCurrentUserId"] = chatAddMap

                            mRootRef.updateChildren(
                                chatUserMap
                            ) { databaseError, _ ->
                                if (databaseError != null) {
                                    Log.d("CHAT_LOG", databaseError.message)
                                }
                            }
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })

            dialog.dismiss()
        }, 2000)

        mChatSendBtn.setOnClickListener { sendMessage() }

        mChatAddBtn.setOnClickListener {
            val galleryIntent = Intent()

            galleryIntent.type = "image/*"
            galleryIntent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(
                Intent.createChooser(galleryIntent, "SELECT IMAGE"),
                GALLERY_PICK
            )
        }

        mRefreshLayout.setOnRefreshListener {
            mCurrentPage++
            itemPos = 0

            loadMoreMessages(messagesList)
        }


        back.setOnClickListener {
            replaceFragment(
                R.id.fragmentContainer,
                ChatFragment(),
                null,
                null
            )
        }
    }

    private fun loadMoreMessages(messagesList: MutableList<Messages>) {
        val mCurrentUserId = FirebaseAuth.getInstance().currentUser!!.uid
        val messageRef =
            FirebaseDatabase.getInstance().reference.child("messages").child(mCurrentUserId)
                .child(mChatUser!!)
        val messageQuery = messageRef.orderByKey().endAt(mLastKey).limitToLast(10)

        messageQuery.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {
                val message =
                    dataSnapshot.getValue(
                        Messages::class.java
                    )!!

                val messageKey = dataSnapshot.key
                if (mPrevKey != messageKey) {
                    messagesList.add(itemPos++, message)
                } else {
                    mPrevKey = mLastKey
                }
                if (itemPos == 1) {
                    mLastKey = messageKey!!
                }
                Log.d(
                    "TOTALKEYS",
                    "Last Key : $mLastKey | Prev Key : $mPrevKey | Message Key : $messageKey"
                )
                (mMessagesList.adapter as MessageAdapter).notifyDataSetChanged()
                mRefreshLayout.isRefreshing = false
                LinearLayoutManager(context).scrollToPositionWithOffset(10, 0)
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, s: String?) {}
            override fun onChildRemoved(dataSnapshot: DataSnapshot) {}
            override fun onChildMoved(dataSnapshot: DataSnapshot, s: String?) {}
            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }


    private fun loadMessages(messagesList: MutableList<Messages>) {
        val mCurrentUserId = FirebaseAuth.getInstance().currentUser!!.uid
        val messageRef =
            FirebaseDatabase.getInstance().reference.child("messages").child(mCurrentUserId)
                .child(mChatUser!!)
        val messageQuery: Query = messageRef.limitToLast(mCurrentPage * TOTAL_ITEMS_TO_LOAD)

        messageQuery.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {
                val message =
                    dataSnapshot.getValue(
                        Messages::class.java
                    )!!
                itemPos++

                if (itemPos == 1) {
                    val messageKey = dataSnapshot.key
                    mLastKey = messageKey!!
                    mPrevKey = messageKey
                }
                messagesList.add(message)
                (mMessagesList.adapter as MessageAdapter).notifyDataSetChanged()

                if (mMessagesList != null) {
                    mMessagesList.scrollToPosition(messagesList.size - 1)
                }
                if (mRefreshLayout != null) mRefreshLayout.isRefreshing = false
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, s: String?) {
                val message =
                    dataSnapshot.getValue(
                        Messages::class.java
                    )!!

                if (message.view == "deleted") {
                    val messageNew = Messages(
                        "Сообщение удалено",
                        message.type,
                        "deleted",
                        message.time,
                        false,
                        message.from
                    )


                    for (index in 0 until messagesList.size) {
                        if (messagesList[index].time == message.time) {
                            messagesList.removeAt(index)
                            messagesList.add(index, messageNew)
                            (mMessagesList.adapter as MessageAdapter).notifyItemChanged(index)
                            break
                        }
                    }


                }
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {}
            override fun onChildMoved(dataSnapshot: DataSnapshot, s: String?) {}
            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }


    private fun sendMessage() {
        val message = mChatMessageView.text.toString()
        val mCurrentUserId = FirebaseAuth.getInstance().currentUser!!.uid
        val mRootRef = FirebaseDatabase.getInstance().reference

        if (!TextUtils.isEmpty(message)) {
            val currentUserRef = "messages/$mCurrentUserId/$mChatUser"
            val chatUserRef = "messages/$mChatUser/$mCurrentUserId"
            val userMessagePush = mRootRef.child("messages")
                .child(mCurrentUserId).child(mChatUser!!).push()
            val pushId = userMessagePush.key

            val messageMap = HashMap<String, Any>() as MutableMap<String, Any>

            messageMap["message"] = message
            messageMap["seen"] = false
            messageMap["type"] = "text"
            messageMap["time"] = ServerValue.TIMESTAMP
            messageMap["from"] = mCurrentUserId
            messageMap["view"] = "success"

            val messageUserMap = HashMap<String, Any>() as MutableMap<String, Any>

            messageUserMap["$currentUserRef/$pushId"] = messageMap
            messageUserMap["$chatUserRef/$pushId"] = messageMap

            mChatMessageView.setText("")
            mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser!!).child("seen")
                .setValue(true)
            mRootRef.child("Chat").child(mCurrentUserId).child(mChatUser!!).child("timestamp")
                .setValue(ServerValue.TIMESTAMP)
            mRootRef.child("Chat").child(mChatUser!!).child(mCurrentUserId).child("seen")
                .setValue(false)
            mRootRef.child("Chat").child(mChatUser!!).child(mCurrentUserId).child("timestamp")
                .setValue(ServerValue.TIMESTAMP)

            mRootRef.updateChildren(
                messageUserMap
            ) { databaseError, _ ->
                if (databaseError != null) {
                    Log.d("CHAT_LOG", databaseError.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val mCurrentUserId = FirebaseAuth.getInstance().currentUser!!.uid
        val mRootRef = FirebaseDatabase.getInstance().reference
        val mImageStorage = FirebaseStorage.getInstance().reference

        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {
            val imageUri: Uri = data!!.data!!
            val currentUserRef = "messages/$mCurrentUserId/$mChatUser"
            val chatUserRef = "messages/$mChatUser/$mCurrentUserId"
            val userMessagePush = mRootRef.child("messages")
                .child(mCurrentUserId).child(mChatUser!!).push()
            val pushId = userMessagePush.key
            val filepath =
                mImageStorage.child("message_images").child("$pushId.jpg")
            filepath.putFile(imageUri)
                .addOnSuccessListener {
                    filepath.downloadUrl
                        .addOnSuccessListener {
                            val url = it
                            val messageMap = HashMap<String, Any>() as MutableMap<String, Any>

                            messageMap["message"] = url.toString()
                            messageMap["seen"] = false
                            messageMap["type"] = "image"
                            messageMap["time"] = ServerValue.TIMESTAMP
                            messageMap["from"] = mCurrentUserId
                            messageMap["view"] = "success"

                            val messageUserMap = HashMap<String, Any>() as MutableMap<String, Any>

                            messageUserMap["$currentUserRef/$pushId"] = messageMap
                            messageUserMap["$chatUserRef/$pushId"] = messageMap

                            mChatMessageView.setText("")
                            mRootRef.updateChildren(
                                messageUserMap
                            ) { databaseError, _ ->
                                if (databaseError != null) {
                                    Log.d(
                                        "CHAT_LOG",
                                        databaseError.message
                                    )
                                }
                            }

                        }


                }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString("chatUser", mChatUser)
    }

    companion object {
        private const val TOTAL_ITEMS_TO_LOAD = 10
        private var mCurrentPage = 1
        private const val GALLERY_PICK = 1
        private var itemPos = 0
        private var mLastKey = ""
        private var mPrevKey = ""


        fun newInstance(uid: String, username: String): ConversationFragment {
            val fragment = ConversationFragment()
            val args = Bundle()

            args.putString("uid", uid)
            args.putString("username", username)
            fragment.arguments = args

            return fragment
        }
    }

}
