package com.furymaxim.chatapp.ui.fragments.main_group


import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.adapters.MessageGroupAdapter
import com.furymaxim.chatapp.models.Messages
import com.furymaxim.chatapp.ui.activities.MainActivity
import com.furymaxim.chatapp.ui.fragments.BaseFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.fragment_group_conv.*

class GroupConvFragment : BaseFragment() {
    private var leaveStatus = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_group_conv, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mAuth = FirebaseAuth.getInstance()
        val mGroupsDatabase = FirebaseDatabase.getInstance().reference.child("Groups")
        val mChatDatabase = FirebaseDatabase.getInstance().reference.child("Chat")
        val messagesList: MutableList<Messages> = ArrayList()
        val mCurrentUserId = mAuth.currentUser!!.uid
        val creator = arguments!!.getString("creator")
        val mGroupId = arguments!!.getString("groupId")
        val groupName = arguments!!.getString("groupName")

        (activity as MainActivity).supportActionBar!!.hide()

        val dialog = SpotsDialog.Builder()
            .setContext(context)
            .setCancelable(false)
            .setTheme(R.style.ProgressDialog)
            .build()

        dialog.show()

        if (creator == mCurrentUserId) {
            leaveChat.text = "Удалить группу"
        } else {
            leaveChat.text = "Выйти из группы"
        }

        val mAdapter = MessageGroupAdapter(messagesList, mGroupId!!)

        mMessagesList.layoutManager = LinearLayoutManager(context)
        mMessagesList.adapter = mAdapter

        Handler(Looper.getMainLooper()).postDelayed({
        groupNameTV.text = groupName

        mChatDatabase.child(mCurrentUserId).child("groups")
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    for (myChild in dataSnapshot.children) {
                        if (myChild.value.toString() == mGroupId) {
                            mChatDatabase.child(mCurrentUserId).child("groups")
                                .child(mGroupId)
                                .child("seen").setValue(true)
                        }
                    }


                }
            })


        loadMessages(messagesList)

        mChatDatabase.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            @SuppressLint("SetTextI18n")
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var count = 0
                for (user in dataSnapshot.children) {
                    for (group in user.child("groups").children) {
                        if (group.key.toString() == mGroupId) {
                            count++

                        }
                    }

                }
                if (usersCount != null) usersCount.text = "($count)"
            }
        })

        mGroupsDatabase.child(mGroupId)
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val image = dataSnapshot.child("image").value.toString()
                    if (imageView != null) {
                        Picasso.get().load(image).placeholder(R.drawable.default_avatar)
                            .into(imageView)
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {}
            })

            dialog.dismiss()
        }, 2000)


        mChatSendBtn.setOnClickListener { sendMessage() }

        mChatAddBtn.setOnClickListener {
            val galleryIntent = Intent()
            galleryIntent.type = "image/*"
            galleryIntent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(
                Intent.createChooser(galleryIntent, "SELECT IMAGE"),
                GALLERY_PICK
            )
        }

        leaveChat.setOnClickListener {
            leaveStatus = true
            if (creator == mCurrentUserId) {
                mChatDatabase.child(mCurrentUserId).child("groups")
                    .addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {

                        }

                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            for (item in dataSnapshot.children) {
                                if (item.key!!.toString() == mGroupId) {
                                    mChatDatabase.child(mCurrentUserId).child("groups")
                                        .child(item.key.toString()).removeValue()
                                        .addOnSuccessListener {
                                            mGroupsDatabase.child(mGroupId).removeValue()
                                                .addOnSuccessListener {
                                                    replaceFragment(
                                                        R.id.fragmentContainer,
                                                        GroupsFragment.newInstance(),
                                                        null,
                                                        null
                                                    )
                                                }
                                        }
                                    break
                                }
                            }
                        }
                    })
            } else {
                mChatDatabase.child(mCurrentUserId).child("groups")
                    .addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {
                        }

                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            for (item in dataSnapshot.children) {
                                if (item.key!!.toString() == mGroupId) {
                                    mChatDatabase.child(mCurrentUserId).child("groups")
                                        .child(item.key.toString()).removeValue()
                                        .addOnSuccessListener {
                                            replaceFragment(
                                                R.id.fragmentContainer,
                                                GroupsFragment.newInstance(),
                                                null,
                                                null
                                            )
                                        }
                                    break
                                }
                            }
                        }
                    })
            }
        }




        inviteFriend.setOnClickListener {
            replaceFragment(
                R.id.fragmentContainer,
                InviteFriendToGroupFragment.newInstance(mGroupId, groupName!!, creator!!),
                null,
                null
            )
        }

        usersCount.setOnClickListener {
            replaceFragment(
                R.id.fragmentContainer,
                GroupMembersFragment.newInstance(mGroupId, groupName!!, creator!!),
                null,
                null
            )
        }

        mRefreshLayout.setOnRefreshListener {
            mCurrentPage++
            itemPos = 0

            loadMoreMessages(messagesList)
        }

        back.setOnClickListener {
            replaceFragment(R.id.fragmentContainer, GroupsFragment(), null, null)
        }
    }


    private fun loadMoreMessages(messagesList: MutableList<Messages>) {
        val mRootRef = FirebaseDatabase.getInstance().reference
        val mGroupId = arguments!!.getString("groupId")
        val messageRef =
            mRootRef.child("messages").child(mGroupId!!)
        val messageQuery = messageRef.orderByKey().endAt(mLastKey).limitToLast(10)

        messageQuery.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {
                val message =
                    dataSnapshot.getValue(
                        Messages::class.java
                    )!!
                if (message.view != "deleted") {
                    val messageKey = dataSnapshot.key
                    if (mPrevKey != messageKey) {
                        messagesList.add(itemPos++, message)
                    } else {
                        mPrevKey = mLastKey
                    }
                    if (itemPos == 1) {
                        mLastKey = messageKey!!
                    }

                    (mMessagesList.adapter as MessageGroupAdapter).notifyDataSetChanged()
                    mRefreshLayout.isRefreshing = false
                    LinearLayoutManager(context).scrollToPositionWithOffset(10, 0)
                }
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, s: String?) {
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {
            }

            override fun onChildMoved(dataSnapshot: DataSnapshot, s: String?) {}
            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }


    private fun loadMessages(messagesList: MutableList<Messages>) {
        val mRootRef = FirebaseDatabase.getInstance().reference
        val mGroupId = arguments!!.getString("groupId")
        val messageRef =
            mRootRef.child("messages").child(mGroupId!!)
        val messageQuery: Query = messageRef.limitToLast(mCurrentPage * TOTAL_ITEMS_TO_LOAD)

        messageQuery.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {
                val message =
                    dataSnapshot.getValue(
                        Messages::class.java
                    )!!
                itemPos++

                if (itemPos == 1) {
                    val messageKey = dataSnapshot.key
                    mLastKey = messageKey!!
                    mPrevKey = messageKey
                }
                messagesList.add(message)
                (mMessagesList.adapter as MessageGroupAdapter).notifyDataSetChanged()
                if (mMessagesList != null) {
                    mMessagesList.scrollToPosition(messagesList.size - 1)
                }
                if (mRefreshLayout != null) mRefreshLayout.isRefreshing = false
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, s: String?) {
                val message =
                    dataSnapshot.getValue(
                        Messages::class.java
                    )!!

                if (message.view == "deleted") {
                    val messageNew = Messages(
                        "Сообщение удалено",
                        message.type,
                        "deleted",
                        message.time,
                        false,
                        message.from
                    )

                    for (index in 0 until messagesList.size) {
                        if (messagesList[index].time == message.time) {
                            messagesList.removeAt(index)
                            messagesList.add(index, messageNew)
                            (mMessagesList.adapter as MessageGroupAdapter).notifyItemChanged(index)
                            break
                        }
                    }
                }
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {}
            override fun onChildMoved(dataSnapshot: DataSnapshot, s: String?) {}
            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    private fun sendMessage() {
        val message = mChatMessageView.text.toString()
        val mRootRef = FirebaseDatabase.getInstance().reference
        val mGroupId = arguments!!.getString("groupId")
        val mCurrentUserId = FirebaseAuth.getInstance().currentUser!!.uid
        val mChatDatabase = FirebaseDatabase.getInstance().reference.child("Chat")
        if (!TextUtils.isEmpty(message)) {
            val userMessagePush = mRootRef.child("messages").child(mGroupId!!).push()
            val pushId = userMessagePush.key
            val messageMap = HashMap<String, Any>() as MutableMap<String, Any>

            messageMap["message"] = message
            messageMap["seen"] = false
            messageMap["type"] = "text"
            messageMap["time"] = ServerValue.TIMESTAMP
            messageMap["from"] = mCurrentUserId

            mChatMessageView.setText("")


            val timestamp = ServerValue.TIMESTAMP

            mChatDatabase.child(mCurrentUserId).child("groups").child(mGroupId).child("seen")
                .setValue(true)
            mChatDatabase.child(mCurrentUserId).child("groups").child(mGroupId).child("timestamp")
                .setValue(timestamp)

            mChatDatabase.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(p0: DataSnapshot) {
                    for (user in p0.children) {
                        if (user.key.toString() != mCurrentUserId) {
                            mChatDatabase.child(user.key.toString()).child("groups").child(mGroupId)
                                .child("seen").setValue(false)
                            mChatDatabase.child(user.key.toString()).child("groups").child(mGroupId)
                                .child("timestamp").setValue(timestamp)
                        }
                    }
                }
            })

            mRootRef.child("messages").child(mGroupId).child(pushId!!).setValue(messageMap)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val mRootRef = FirebaseDatabase.getInstance().reference
        val mGroupId = arguments!!.getString("groupId")
        val mImageStorage = FirebaseStorage.getInstance().reference
        val mCurrentUserId = FirebaseAuth.getInstance().currentUser!!.uid

        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {
            val imageUri: Uri = data!!.data!!
            val userMessagePush = mRootRef.child("messages")
                .child(mGroupId!!).push()
            val pushId = userMessagePush.key
            val filepath =
                mImageStorage.child("message_images").child("$pushId.jpg")

            filepath.putFile(imageUri)
                .addOnSuccessListener {

                    filepath.downloadUrl
                        .addOnSuccessListener {
                            val url = it

                            val messageMap = HashMap<String, Any>() as MutableMap<String, Any>

                            messageMap["message"] = url.toString()
                            messageMap["seen"] = false
                            messageMap["type"] = "image"
                            messageMap["time"] = ServerValue.TIMESTAMP
                            messageMap["from"] = mCurrentUserId
                            messageMap["view"] = "success"

                            val messageUserMap =
                                HashMap<String, Any>() as MutableMap<String, Any>

                            messageUserMap["$userMessagePush/$pushId"] = messageMap

                            mChatMessageView.setText("")
                            mRootRef.updateChildren(
                                messageUserMap
                            ) { databaseError, _ ->
                                if (databaseError != null) {
                                    Log.d(
                                        "CHAT_LOG",
                                        databaseError.message
                                    )
                                }
                            }

                        }
                }
        }
    }

    companion object {
        private const val TOTAL_ITEMS_TO_LOAD = 10
        private var mCurrentPage = 1
        private const val GALLERY_PICK = 1
        private var itemPos = 0
        private var mLastKey = ""
        private var mPrevKey = ""

        fun newInstance(groupId: String, groupName: String, creator: String): GroupConvFragment {
            val fragment = GroupConvFragment()
            val args = Bundle()

            args.putString("groupId", groupId)
            args.putString("groupName", groupName)
            args.putString("creator", creator)
            fragment.arguments = args

            return fragment
        }
    }


}
