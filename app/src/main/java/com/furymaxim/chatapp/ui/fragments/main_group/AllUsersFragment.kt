package com.furymaxim.chatapp.ui.fragments.main_group

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.models.User
import com.furymaxim.chatapp.ui.activities.MainActivity
import com.furymaxim.chatapp.ui.fragments.BaseFragment
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.fragment_all_users.*
import kotlinx.android.synthetic.main.fragment_settings.back

class AllUsersFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_all_users, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).supportActionBar!!.hide()

        back.setOnClickListener {
            replaceFragment(R.id.fragmentContainer, ChatFragment(), null, null)
        }
    }

    override fun onStart() {
        super.onStart()

        val mUsersDatabase = FirebaseDatabase.getInstance().reference.child("Users")

        val options: FirebaseRecyclerOptions<User> = FirebaseRecyclerOptions.Builder<User>()
            .setQuery(mUsersDatabase, User::class.java)
            .build()

        val adapter = MyFirebaseRecyclerAdapter(options)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        adapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        (recyclerView.adapter as MyFirebaseRecyclerAdapter).stopListening()
    }


    inner class MyFirebaseRecyclerAdapter internal constructor(options: FirebaseRecyclerOptions<User>) :
        FirebaseRecyclerAdapter<User, UserViewHolder>(options) {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
            return UserViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.single_user_layout,
                    parent,
                    false
                )
            )
        }

        override fun onBindViewHolder(holder: UserViewHolder, position: Int, model: User) {
            val userID = getRef(position).key

            holder.setName(model.name)
            holder.setStatus(model.status)
            holder.setImage(model.image)
            holder.view.setOnClickListener {
                replaceFragment(
                    R.id.fragmentContainer,
                    ProfileFragment.newInstance(userID!!),
                    null,
                    null
                )
            }
        }
    }

    inner class UserViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        private var image: ImageView? = null
        private var status: TextView? = null
        private var name: TextView? = null

        init {
            image = view.findViewById(R.id.thumbnail)
            status = view.findViewById(R.id.status)
            name = view.findViewById(R.id.username)
        }

        fun setName(username: String) {
            name!!.text = username
        }

        fun setStatus(userStatus: String) {
            status!!.text = userStatus
        }

        fun setImage(imageLink: String) {
            if (imageLink != "default") {
                Glide
                    .with(context!!)
                    .load(imageLink)
                    .into(image!!)
            }
        }
    }


}
