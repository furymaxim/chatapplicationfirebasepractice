package com.furymaxim.chatapp.ui.fragments

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment

abstract class BaseFragment: Fragment() {

    protected fun replaceFragment(
        @IdRes containerViewId: Int,
        fragment: Fragment,
        fragmentTag: String?,
        backStackStateName: String?
    ) {
        fragmentManager!!
            .beginTransaction()
            .replace(containerViewId, fragment, fragmentTag)
            .addToBackStack(backStackStateName)
            .commit()
    }
}