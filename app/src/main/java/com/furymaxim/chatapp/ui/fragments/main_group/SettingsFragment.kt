package com.furymaxim.chatapp.ui.fragments.main_group


import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.ui.activities.MainActivity
import com.furymaxim.chatapp.ui.fragments.BaseFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.dialog_photo_camera.view.*
import kotlinx.android.synthetic.main.fragment_settings.*


class SettingsFragment : BaseFragment() {

    private val mFirebaseDatabase = FirebaseDatabase.getInstance()
    private val mAuth = FirebaseAuth.getInstance()
    private val mStorageRef = FirebaseStorage.getInstance().reference

    private var which = 0
    private var currentStatus: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    @SuppressLint("InflateParams")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).supportActionBar!!.hide()

        if (savedInstanceState != null) {
            currentStatus = savedInstanceState.getString("status")
        }

        back.setOnClickListener {
            replaceFragment(R.id.fragmentContainer, ChatFragment(), null, null)
        }

        settingsChangeStatusBtn.setOnClickListener {
            which = 1

            currentStatus = settingsStatus.text.toString()

            settingsStatus.visibility = View.GONE
            settingsEditStatus.visibility = View.VISIBLE
            settingsEditStatus.setText(currentStatus)

            settingsChangeImgBtn.visibility = View.GONE
            settingsChangeStatusBtn.visibility = View.GONE

            settingsSaveChangesBtn.visibility = View.VISIBLE
        }

        settingsChangeImgBtn.setOnClickListener {
            which = 2

            val mDialogView =
                LayoutInflater.from(context).inflate(R.layout.dialog_photo_camera, null)
            val mBuilder = AlertDialog.Builder(context!!, R.style.CustomDialog)
                .setView(mDialogView)
            val mAlertDialog = mBuilder.create()

            mAlertDialog.show()

            mAlertDialog.setCanceledOnTouchOutside(true)
            mAlertDialog.setCancelable(true)

            mDialogView.openCameraBtn.setOnClickListener {
                mAlertDialog.dismiss()
            }

            mDialogView.openGalleryBtn.setOnClickListener {
                mAlertDialog.dismiss()

                val galleryIntent = Intent()

                galleryIntent.type = "image/*"
                galleryIntent.action = Intent.ACTION_GET_CONTENT

                startActivityForResult(
                    Intent.createChooser(galleryIntent, "Выбери изображение"),
                    REQUEST_CODE
                )
            }
        }


        settingsSaveChangesBtn.setOnClickListener {
            if (which == 1) {
                val newStatus = settingsEditStatus.text.toString()

                if (newStatus.isNotEmpty()) {
                    progressStatusLayout.visibility = View.VISIBLE
                    if (newStatus != currentStatus) {
                        mFirebaseDatabase.reference
                            .child("Users")
                            .child(mAuth.currentUser!!.uid)
                            .child("status")
                            .setValue(newStatus)

                        settingsSaveChangesBtn.visibility = View.GONE
                        settingsEditStatus.visibility = View.GONE

                        Handler().postDelayed({
                            progressStatusLayout.visibility = View.GONE
                            settingsChangeStatusBtn.visibility = View.VISIBLE
                            settingsChangeImgBtn.visibility = View.VISIBLE
                            settingsStatus.text = newStatus
                            settingsStatus.visibility = View.VISIBLE
                        }, 500)
                    } else {
                        settingsSaveChangesBtn.visibility = View.GONE
                        settingsEditStatus.visibility = View.GONE

                        Handler().postDelayed({
                            progressStatusLayout.visibility = View.GONE
                            settingsChangeStatusBtn.visibility = View.VISIBLE
                            settingsChangeImgBtn.visibility = View.VISIBLE
                            settingsStatus.text = newStatus
                            settingsStatus.visibility = View.VISIBLE
                        }, 500)
                    }
                } else {
                    Toast.makeText(context, "Поле не может быть пустым.", Toast.LENGTH_SHORT).show()
                }
            }
        }

        val myRef = mFirebaseDatabase.reference
            .child("Users")
            .child(mAuth.currentUser!!.uid)

        myRef.keepSynced(true)

        myRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                val name = p0.child("name").value.toString()
                val status = p0.child("status").value.toString()
                val avatar = p0.child("image").value.toString()

                if (avatar != "default") {
                    if (settingsImage != null) {
                        Glide
                            .with(context!!)
                            .load(avatar)
                            .placeholder(R.drawable.default_avatar)
                            .diskCacheStrategy(DiskCacheStrategy.DATA)
                            .into(settingsImage)

                        if (settingsImage.visibility == View.INVISIBLE && progressImageLayout.visibility == View.VISIBLE)
                            progressImageLayout.visibility = View.GONE
                    }
                } else {
                    if (settingsImage != null) {
                        settingsImage.setImageDrawable(
                            resources.getDrawable(
                                R.drawable.default_avatar,
                                null
                            )
                        )
                    }
                }

                Handler(Looper.getMainLooper()).postDelayed({
                    if (progressLayout != null) progressLayout.visibility = View.GONE
                    if (settingsStatus != null) settingsStatus.visibility = View.VISIBLE
                    if (settingsDisplayName != null) settingsDisplayName.visibility = View.VISIBLE
                    if (settingsChangeImgBtn != null) settingsChangeImgBtn.visibility = View.VISIBLE
                    if (settingsChangeStatusBtn != null) settingsChangeStatusBtn.visibility =
                        View.VISIBLE
                    if (settingsImage != null) settingsImage.visibility = View.VISIBLE
                    if (settingsDisplayName != null) settingsDisplayName.text = name
                    if (settingsStatus != null) settingsStatus.text = status
                }, 200)


            }
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            val imageUri = data!!.data!!

            CropImage.activity(imageUri)
                .setAspectRatio(1, 1)
                .start(context!!, this)
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {
                val resultUri = result.uri
                val filePath = mStorageRef
                    .child("profile_images")
                    .child("${mAuth.currentUser!!.uid}.jpg")

                settingsImage.visibility = View.INVISIBLE
                progressImageLayout.visibility = View.VISIBLE

                filePath.putFile(resultUri)
                    .addOnSuccessListener {
                        filePath.downloadUrl.addOnSuccessListener {
                            val url = it

                            mFirebaseDatabase.reference
                                .child("Users")
                                .child(mAuth.currentUser!!.uid)
                                .child("image")
                                .setValue(url.toString())
                        }

                    }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error

                settingsImage.visibility = View.VISIBLE
                progressImageLayout.visibility = View.GONE
                Toast.makeText(
                    context,
                    "Произошла ошибка. Попробуйте обновить аватар спустя некоторое время. $error",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString("status", currentStatus)
    }


    companion object {
        private const val REQUEST_CODE = 101
    }

}
