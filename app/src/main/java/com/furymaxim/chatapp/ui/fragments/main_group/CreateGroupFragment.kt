package com.furymaxim.chatapp.ui.fragments.main_group


import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.adapters.GroupChatRequestAdapter
import com.furymaxim.chatapp.models.User
import com.furymaxim.chatapp.ui.fragments.BaseFragment
import com.furymaxim.chatapp.utils.OnItemCheckListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.fragment_create_group.*
import java.util.*
import kotlin.collections.HashMap


class CreateGroupFragment : BaseFragment(), OnItemCheckListener {

    private var checkedFriendsList: ArrayList<User> = ArrayList()

    private var itemPos = 0
    private var imageLink = "default"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_create_group, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mCurrentUserId = FirebaseAuth.getInstance().currentUser!!.uid
        val friendsList: MutableList<User> = ArrayList()

        val mAdapter = GroupChatRequestAdapter(friendsList, this)

        usersList.layoutManager = LinearLayoutManager(context)

        usersList.adapter = mAdapter

        loadUsersToInvite(friendsList)

        if (savedInstanceState != null) {
            checkedFriendsList = savedInstanceState.getParcelableArrayList("list")!!
        }

        addImage.setOnClickListener {
            val galleryIntent = Intent()

            galleryIntent.type = "image/*"
            galleryIntent.action = Intent.ACTION_GET_CONTENT

            startActivityForResult(
                Intent.createChooser(galleryIntent, "Выбери изображение"),
                REQUEST_CODE
            )
        }

        FirebaseDatabase.getInstance().reference
            .child(mCurrentUserId)
            .child("image")
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(p0: DataSnapshot) {
                    if (p0.exists()) {
                        if (context != null && groupImage != null) {
                            val dialog = SpotsDialog.Builder()
                                .setContext(context)
                                .setCancelable(false)
                                .setTheme(R.style.ProgressDialogImage)
                                .build()

                            dialog.show()

                            Handler(Looper.getMainLooper()).postDelayed({
                            imageLink = p0.value.toString()
                            Picasso.get().load(p0.value.toString())
                                .placeholder(R.drawable.default_avatar).into(groupImage)
                                dialog.dismiss()
                            }, 2000)
                        }
                    }
                }
            })

        createGroup.setOnClickListener {
            if (nameEdit.text.toString().length > 2) {
                val groupPush = FirebaseDatabase.getInstance().reference.child("Groups").push()
                val pushId = groupPush.key
                val groupMap = HashMap<String, Any>() as MutableMap<String, Any>

                groupMap["name"] = nameEdit.text.toString()
                groupMap["creator"] = mCurrentUserId
                groupMap["time"] = ServerValue.TIMESTAMP
                groupMap["image"] = imageLink

                FirebaseDatabase.getInstance().reference.child("Groups").child(pushId!!)
                    .setValue(groupMap)

                var groupChatMap = HashMap<String, Any>() as MutableMap<String, Any>

                groupChatMap["seen"] = false
                groupChatMap["time"] = 0

                FirebaseDatabase.getInstance().reference
                    .child("Chat").child(mCurrentUserId).child("groups").child(pushId)
                    .setValue(groupChatMap)

                if (checkedFriendsList.size > 0) {
                    for (friend in checkedFriendsList) {
                        FirebaseDatabase.getInstance().reference.child("Users")
                            .addValueEventListener(object : ValueEventListener {
                                override fun onDataChange(dataSnapshot: DataSnapshot) {
                                    for (item in dataSnapshot.children) {
                                        if (item!!.child("name").value == friend.name) {
                                            groupChatMap =
                                                HashMap()
                                            groupChatMap["seen"] = false
                                            groupChatMap["time"] = 0

                                            FirebaseDatabase.getInstance().reference
                                                .child("Chat").child(item.key!!.toString())
                                                .child("groups").child(pushId)
                                                .setValue(groupChatMap)

                                            FirebaseDatabase.getInstance()
                                                .reference.child("Groups_invites")
                                                .child(item.key.toString())
                                                .child("state").setValue("active")
                                        }
                                    }

                                }

                                override fun onCancelled(p0: DatabaseError) {

                                }
                            })

                    }
                }
                replaceFragment(
                    R.id.fragmentContainer,
                    GroupsFragment.newInstance(),
                    null,
                    null
                )

            } else {
                Toast.makeText(context!!, "Введите имя группы", Toast.LENGTH_SHORT).show()
            }

        }
    }

    private fun loadUsersToInvite(friendsList: MutableList<User>) {
        val mCurrentUserId = FirebaseAuth.getInstance().currentUser!!.uid

        FirebaseDatabase.getInstance().reference.child("Friends").child(mCurrentUserId)
            .addChildEventListener(object : ChildEventListener {
                override fun onCancelled(p0: DatabaseError) {}
                override fun onChildMoved(p0: DataSnapshot, p1: String?) {}

                override fun onChildChanged(p0: DataSnapshot, p1: String?) {}

                override fun onChildAdded(dataSnapshot: DataSnapshot, p1: String?) {
                    val friendId = dataSnapshot.key.toString()

                    FirebaseDatabase.getInstance().reference.child("Users").child(friendId)
                        .addValueEventListener(object : ValueEventListener {
                            override fun onCancelled(p0: DatabaseError) {}

                            override fun onDataChange(p0: DataSnapshot) {
                                val userName = p0.child("name").value.toString()
                                val userImage = p0.child("image").value.toString()
                                val status = p0.child("status").value.toString()

                                friendsList.add(itemPos++, User(userName, status, userImage))

                                (usersList.adapter as GroupChatRequestAdapter).notifyDataSetChanged()
                            }
                        })
                }
                override fun onChildRemoved(p0: DataSnapshot) {}
            })
    }


    override fun onStop() {
        super.onStop()
        val mCurrentUserId = FirebaseAuth.getInstance().currentUser!!.uid

        FirebaseDatabase.getInstance().reference.child(mCurrentUserId).removeValue()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val mCurrentUserId = FirebaseAuth.getInstance().currentUser!!.uid

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {

            val imageUri = data!!.data!!

            CropImage.activity(imageUri)
                .setAspectRatio(1, 1)
                .start(context!!, this)
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {

                val resultUri = result.uri

                val randomUUID = name.text.toString()

                val filePath = FirebaseStorage.getInstance().reference
                    .child("group_images")
                    .child(randomUUID)
                    .child("${randomUUID}.jpg")

                filePath.putFile(resultUri)
                    .addOnSuccessListener {
                        filePath.downloadUrl.addOnSuccessListener {
                            val url = it

                            FirebaseDatabase.getInstance().reference
                                .child(mCurrentUserId)
                                .child("image")
                                .setValue(url.toString())
                        }
                    }
            }
        }
    }

    override fun onItemCheck(item: User) {
        checkedFriendsList.add(item)
    }

    override fun onItemUncheck(item: User) {
        checkedFriendsList.remove(item)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList("list", checkedFriendsList)
    }

    companion object {
        private const val REQUEST_CODE = 101

        fun newInstance(): CreateGroupFragment {
            return CreateGroupFragment()
        }
    }


}
