package com.furymaxim.chatapp.ui.activities

import android.os.Bundle
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.ui.fragments.main_group.ChatFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(mainAppBar as androidx.appcompat.widget.Toolbar)

        replaceFragment(
            R.id.fragmentContainer,
            ChatFragment(), null, null
        )
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            if (supportFragmentManager.backStackEntryCount == 1) {
                finish()
            } else
                supportFragmentManager.popBackStack()
        } else
            finish()
    }

}
