package com.furymaxim.chatapp.ui.fragments.registration_group


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.ui.activities.MainActivity
import com.furymaxim.chatapp.ui.activities.StartActivity
import com.furymaxim.chatapp.ui.fragments.BaseFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.fragment_registration.*


class RegistrationFragment : BaseFragment() {

    private var mAuth = FirebaseAuth.getInstance()
    private var mFirebaseDatabase = FirebaseDatabase.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        back.setOnClickListener {
            replaceFragment(R.id.fragmentContainer, StartFragment(), null, null)
        }

        createAccountBtn.setOnClickListener {
            if (regDisplayName.text.isNotEmpty() && regEmail.text.isNotEmpty() && regPassword.text.isNotEmpty()) {

                val inputManager: InputMethodManager =
                    context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(
                    createAccountBtn.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )

                progressLayout.visibility = View.VISIBLE

                Handler().postDelayed({
                    registerUser(
                        regDisplayName.text.toString(),
                        regEmail.text.toString(),
                        regPassword.text.toString()
                    )
                }, 1000)

            }
        }
    }

    private fun registerUser(name: String, email: String, password: String) {
        val mSharedPreferences = context!!.getSharedPreferences("loginPref", Context.MODE_PRIVATE)

        mAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(
                activity as StartActivity
            ) {
                if (it.isSuccessful) {
                    val user = mAuth.currentUser!!
                    val myRef = mFirebaseDatabase.reference
                        .child("Users")
                        .child(user.uid)
                    val userMap = HashMap<String, String>()

                    progressLayout.visibility = View.GONE

                    userMap["name"] = name
                    userMap["status"] = "Всем привет!"
                    userMap["image"] = "default"
                    userMap["thumb_image"] = "default"

                    myRef.setValue(userMap)
                        .addOnSuccessListener {
                            mSharedPreferences!!.edit().putString("email", email).apply()

                            val mainIntent = Intent(
                                activity as StartActivity,
                                MainActivity::class.java
                            )

                            mainIntent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(mainIntent)
                            (activity as StartActivity).finish()
                        }
                } else {
                    Log.w(TAG, "createUserWithEmail:failure", it.exception)
                    progressLayout.visibility = View.GONE
                    Toast.makeText(
                        context,
                        "Возникла ошибка. Попробуй позднее. Причина: ${it.exception.toString()}",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            }
    }

    companion object {

        private const val TAG = "registration"
    }


}
