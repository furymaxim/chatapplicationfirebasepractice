package com.furymaxim.chatapp.ui.fragments.main_group


import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.ui.activities.MainActivity
import com.furymaxim.chatapp.ui.activities.StartActivity
import com.furymaxim.chatapp.ui.fragments.BaseFragment
import com.furymaxim.chatapp.ui.fragments.main_group.tabs.ChatsTabFragment
import com.furymaxim.chatapp.ui.fragments.main_group.tabs.FriendsTabFragment
import com.furymaxim.chatapp.ui.fragments.main_group.tabs.RequestsTabFragment
import com.google.android.material.tabs.TabLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_chat.*

class ChatFragment : BaseFragment() {
    private var lastSelect = 1
    private var count = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).supportActionBar!!.show()
        (activity as MainActivity).supportActionBar!!.title = "My Chat"

        val adapter = PagerAdapter(childFragmentManager)

        setHasOptionsMenu(true)

        pager.adapter = adapter
        setupTabLayout()

        pager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                update(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
        pager.currentItem = lastSelect
    }

    override fun onStart() {
        super.onStart()
        val currentUser = FirebaseAuth.getInstance().currentUser

        if (currentUser == null) {
            sendToStart()
        } else {
            FirebaseDatabase.getInstance().reference.child("Users")
                .child(currentUser.uid).child("online").setValue(true)
        }

    }

    override fun onStop() {
        super.onStop()
        val currentUser = FirebaseAuth.getInstance().currentUser

        if (FirebaseAuth.getInstance().currentUser != null) {
            FirebaseDatabase.getInstance().reference.child("Users")
                .child(currentUser!!.uid).child("online").setValue(ServerValue.TIMESTAMP)
        }
    }

    private fun update(position: Int) {
        val mTitles = resources.getStringArray(R.array.tabs)
        for (i in mTitles.indices) {
            tabs!!.getTabAt(i)!!.customView!!.findViewById<TextView>(R.id.text).isSelected = false

            if (i == position) tabs!!.getTabAt(i)!!.customView!!.findViewById<TextView>(R.id.text)
                .isSelected = true

        }
        lastSelect = position

    }


    private fun setupTabLayout() {
        val mTitles = resources.getStringArray(R.array.tabs)
        for (current in mTitles.indices) {
            val tab: TabLayout.Tab = tabs!!.newTab()

            tab.setCustomView(R.layout.tabs)

            (tab.customView as LinearLayout).findViewById<TextView>(R.id.text).text =
                mTitles[current]

            if (FirebaseAuth.getInstance().currentUser != null) {
                val currentUserId = FirebaseAuth.getInstance().currentUser!!.uid

                FirebaseDatabase.getInstance().reference.child("Friend_req")
                    .addValueEventListener(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {
                        }

                        override fun onDataChange(p0: DataSnapshot) {
                            if (count == 0) (tab.customView as LinearLayout).findViewById<TextView>(
                                R.id.notif
                            ).visibility = View.GONE
                            if (p0.exists()) {
                                for (snapshot in p0.child(currentUserId).children) {
                                    if (snapshot.child("request_type").value.toString() == "received") {
                                        count += 1
                                    } else {
                                        count = 0
                                    }
                                }
                                if (current == 0 && count > 0) {
                                    (tab.customView as LinearLayout).findViewById<TextView>(R.id.notif)
                                        .visibility = View.VISIBLE
                                    (tab.customView as LinearLayout).findViewById<TextView>(R.id.notif)
                                        .text = count.toString()
                                }
                            } else {
                                count = 0
                                (tab.customView as LinearLayout).findViewById<TextView>(R.id.notif)
                                    .text = ""
                                (tab.customView as LinearLayout).findViewById<TextView>(R.id.notif)
                                    .visibility = View.GONE
                            }
                        }
                    })
            }
            tab.customView!!.setOnClickListener { pager!!.currentItem = current }

            tabs!!.addTab(tab)
        }
    }

    override fun onResume() {
        super.onResume()
        update(pager!!.currentItem)
    }

    inner class PagerAdapter(fm: FragmentManager?) :
        FragmentPagerAdapter(fm!!, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        private val mTitles: Array<String> = resources.getStringArray(R.array.tabs)

        override fun getPageTitle(position: Int): CharSequence? {
            return mTitles[position]
        }

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> RequestsTabFragment()
                1 -> ChatsTabFragment()
                else -> FriendsTabFragment()
            }
        }

        override fun getCount(): Int {
            return mTitles.size
        }
    }

    private fun sendToStart() {
        val startIntent = Intent(activity as MainActivity, StartActivity::class.java)

        startActivity(startIntent)
        (activity as MainActivity).finish()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.main_menu, menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val currentUser = FirebaseAuth.getInstance().currentUser

        when (item.itemId) {
            R.id.mainLogOutBtn -> {
                if (currentUser != null) {
                    FirebaseDatabase.getInstance().reference.child("Users")
                        .child(currentUser.uid).child("online").setValue(ServerValue.TIMESTAMP)
                }
                FirebaseAuth.getInstance().signOut()
                sendToStart()
            }
            R.id.mainAccountSettings -> {
                replaceFragment(R.id.fragmentContainer, SettingsFragment(), null, null)
            }
            R.id.mainAllUsers -> {
                replaceFragment(R.id.fragmentContainer, AllUsersFragment(), null, null)
            }
        }
        return true
    }

}
