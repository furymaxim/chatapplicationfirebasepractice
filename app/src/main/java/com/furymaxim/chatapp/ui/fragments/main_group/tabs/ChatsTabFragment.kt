package com.furymaxim.chatapp.ui.fragments.main_group.tabs

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.models.Conv
import com.furymaxim.chatapp.ui.fragments.main_group.ConversationFragment
import com.furymaxim.chatapp.ui.fragments.main_group.GroupsFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_chats_tab.*


class ChatsTabFragment : Fragment() {

    private var mConvDatabase: DatabaseReference? = null
    private var mMessageDatabase: DatabaseReference? = null
    private var mUsersDatabase: DatabaseReference? = null
    private var mGroupReqDatabase: DatabaseReference? = null
    private var mAuth: FirebaseAuth? = null
    private var adapter: MyFirebaseRecyclerAdapter? = null
    private var mCurrentUserId: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_chats_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAuth = FirebaseAuth.getInstance()
        mCurrentUserId = mAuth!!.currentUser!!.uid
        mConvDatabase =
            FirebaseDatabase.getInstance().reference.child("Chat").child(mCurrentUserId!!)

        mConvDatabase!!.keepSynced(true)
        mUsersDatabase = FirebaseDatabase.getInstance().reference.child("Users")
        mMessageDatabase =
            FirebaseDatabase.getInstance().reference.child("messages").child(mCurrentUserId!!)
        mUsersDatabase!!.keepSynced(true)
        mGroupReqDatabase = FirebaseDatabase.getInstance().reference.child("Groups_invites")


        mGroupReqDatabase!!.child(mCurrentUserId!!).addValueEventListener(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                if(p0.child("state").value.toString() == "active"){
                    newGroupIcon.visibility = View.VISIBLE
                }else{
                    if(newGroupIcon !=null) {
                        newGroupIcon.visibility = View.INVISIBLE
                    }
                }
            }
        })


        groupButton.setOnClickListener {
            mGroupReqDatabase!!.child(mCurrentUserId!!).child("state").setValue("inactive")

            val fragmentManager = activity!!.supportFragmentManager
                    fragmentManager.beginTransaction()
                        .replace(
                            R.id.fragmentContainer,
                            GroupsFragment.newInstance()
                        )
                        .addToBackStack(null)
                        .commit()
        }

        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.reverseLayout = true
        linearLayoutManager.stackFromEnd = true

        mConvList.setHasFixedSize(true)
        mConvList.layoutManager = linearLayoutManager
    }

    override fun onStart() {
        super.onStart()

        val options: FirebaseRecyclerOptions<Conv> = FirebaseRecyclerOptions.Builder<Conv>()
            .setQuery(mConvDatabase!!.orderByChild("timestamp"), Conv::class.java)
            .build()

        adapter = MyFirebaseRecyclerAdapter(options)

        mConvList.adapter = adapter

        adapter!!.startListening()
    }


    inner class MyFirebaseRecyclerAdapter internal constructor(options: FirebaseRecyclerOptions<Conv>) :
        FirebaseRecyclerAdapter<Conv, ConvViewHolder>(options) {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConvViewHolder {
            return ConvViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.single_user_layout,
                    parent,
                    false
                )
            )
        }

        override fun onBindViewHolder(holder: ConvViewHolder, position: Int, model: Conv) {

            val listUserId = getRef(position).key

            if(listUserId == "groups")
                holder.itemView.visibility = View.GONE
            else {
                val lastMessageQuery = mMessageDatabase!!.child(listUserId!!).limitToLast(1)

                lastMessageQuery.addChildEventListener(object : ChildEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                    }

                    override fun onChildMoved(p0: DataSnapshot, p1: String?) {

                    }

                    override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                    }

                    override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                        val data: String = p0.child("message").value.toString()
                        val view: String = p0.child("view").value.toString()
                        if (view == "deleted") {
                            holder.setMessage("Сообщение удалено", model.isSeen)
                        } else {
                            holder.setMessage(data, model.isSeen)
                        }
                    }

                    override fun onChildRemoved(p0: DataSnapshot) {
                    }


                })


                mUsersDatabase!!.child(listUserId)
                    .addValueEventListener(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {

                        }

                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            val userName: String = dataSnapshot.child("name").value.toString()
                            val userImage: String =
                                dataSnapshot.child("image").value.toString()

                            if (dataSnapshot.hasChild("online")) {
                                val userOnline: String =
                                    dataSnapshot.child("online").value.toString()
                                holder.setUserOnline(userOnline)
                            }

                            holder.setName(userName)
                            holder.setImage(userImage)

                            holder.view.setOnClickListener {
                                val fragmentManager = activity!!.supportFragmentManager
                                fragmentManager.beginTransaction()
                                    .replace(
                                        R.id.fragmentContainer,
                                        ConversationFragment.newInstance(listUserId, userName)
                                    )
                                    .addToBackStack(null)
                                    .commit()
                            }
                        }
                    })

            }
        }
    }

    inner class ConvViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        private var image: ImageView? = null
        private var status: TextView? = null
        private var name: TextView? = null
        private var userOnline: ImageView? = null

        init {
            image = view.findViewById(R.id.thumbnail)
            status = view.findViewById(R.id.status)
            name = view.findViewById(R.id.username)
            userOnline = view.findViewById(R.id.userOnline)
        }

        fun setName(username: String) {
            name!!.text = username
        }

        fun setMessage(userStatus: String, isSeen: Boolean) {
            status!!.text = userStatus
            if (!isSeen) {
                status!!.setTypeface(status!!.typeface, Typeface.BOLD)
            } else {
                status!!.setTypeface(status!!.typeface, Typeface.NORMAL)
            }
        }

        fun setImage(imageLink: String) {
            Picasso.get().load(imageLink).placeholder(R.drawable.default_avatar).into(image!!)
        }

        fun setUserOnline(status: String) {
            if (status == "true") {
                userOnline!!.visibility = View.VISIBLE
            } else {
                userOnline!!.visibility = View.INVISIBLE
            }
        }


    }

    override fun onStop() {
        super.onStop()

        adapter!!.stopListening()
    }

}
