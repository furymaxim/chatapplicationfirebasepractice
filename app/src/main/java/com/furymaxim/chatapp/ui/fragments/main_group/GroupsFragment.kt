package com.furymaxim.chatapp.ui.fragments.main_group


import android.annotation.SuppressLint
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.furymaxim.chatapp.R
import com.furymaxim.chatapp.models.GroupConv
import com.furymaxim.chatapp.ui.activities.MainActivity
import com.furymaxim.chatapp.ui.fragments.BaseFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_groups.*


class GroupsFragment : BaseFragment() {

    private val mCurrentUserId = FirebaseAuth.getInstance().currentUser!!.uid
    private val mGroupsDatabase = FirebaseDatabase.getInstance().reference.child("Groups")
    private val mMessageDatabase =
        FirebaseDatabase.getInstance().reference.child("messages")
    private val mConvDatabase =
        FirebaseDatabase.getInstance().reference
            .child("Chat")
            .child(mCurrentUserId)
            .child("groups")


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_groups, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val linearLayoutManager = LinearLayoutManager(context)

        (activity as MainActivity).supportActionBar!!.hide()

        mConvDatabase.keepSynced(true)
        mGroupsDatabase.keepSynced(true)

        linearLayoutManager.reverseLayout = true
        linearLayoutManager.stackFromEnd = true

        mConvList.setHasFixedSize(true)
        mConvList.layoutManager = linearLayoutManager

        createChatButton.setOnClickListener {
            replaceFragment(R.id.fragmentContainer, CreateGroupFragment.newInstance(), null, null)
        }

        back.setOnClickListener {
            replaceFragment(R.id.fragmentContainer, ChatFragment(), null, null)
        }
    }

    override fun onStart() {
        super.onStart()

        val options: FirebaseRecyclerOptions<GroupConv> =
            FirebaseRecyclerOptions.Builder<GroupConv>()
                .setQuery(mConvDatabase.orderByChild("timestamp"), GroupConv::class.java)
                .build()

        val adapter = MyFirebaseRecyclerAdapter(options)

        mConvList.adapter = adapter

        adapter.startListening()
    }

    override fun onStop() {
        super.onStop()

        (mConvList.adapter as MyFirebaseRecyclerAdapter).stopListening()
    }


    inner class MyFirebaseRecyclerAdapter internal constructor(options: FirebaseRecyclerOptions<GroupConv>) :
        FirebaseRecyclerAdapter<GroupConv, ConvViewHolder>(options) {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConvViewHolder {
            return ConvViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.group_chat_layout,
                    parent,
                    false
                )
            )
        }

        override fun onBindViewHolder(holder: ConvViewHolder, position: Int, model: GroupConv) {
            val id = getRef(position).key

            val lastMessageQuery = mMessageDatabase.child(id!!).limitToLast(1)

            lastMessageQuery.addChildEventListener(object : ChildEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onChildMoved(p0: DataSnapshot, p1: String?) {

                }

                override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                }

                override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                    val data: String = p0.child("message").value.toString()
                    val view: String = p0.child("view").value.toString()
                    if (view == "deleted") {
                        holder.setMessage("Сообщение удалено", model.isSeen)
                    } else {
                        holder.setMessage(data, model.isSeen)
                    }
                }

                override fun onChildRemoved(p0: DataSnapshot) {
                }

            })


            mGroupsDatabase.child(id).addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    val name: String = dataSnapshot.child("name").value.toString()
                    val image: String = dataSnapshot.child("image").value.toString()
                    val creator: String = dataSnapshot.child("creator").value.toString()

                    holder.setName(name)
                    holder.setImage(image)
                    holder.setGroupRole(creator)

                    holder.view.setOnClickListener {
                        val fragmentManager = activity!!.supportFragmentManager
                        fragmentManager.beginTransaction()
                            .replace(
                                R.id.fragmentContainer,
                                GroupConvFragment.newInstance(id, name, creator)
                            )
                            .commit()
                    }
                }
            })


        }

    }

    inner class ConvViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        private var image: ImageView? = null
        private var name: TextView? = null
        private var role: TextView? = null
        private var messageTV: TextView? = null


        init {
            image = view.findViewById(R.id.groupImage)
            name = view.findViewById(R.id.groupName)
            role = view.findViewById(R.id.groupRole)
            messageTV = view.findViewById(R.id.groupMessage)
        }

        @SuppressLint("SetTextI18n")
        fun setGroupRole(creator: String) {
            if (creator == mCurrentUserId) {
                role!!.text = "Admin"
            } else {
                role!!.text = ""
            }
        }

        fun setMessage(message: String, isSeen: Boolean) {
            messageTV!!.text = message
            if (!isSeen) {
                messageTV!!.setTypeface(messageTV!!.typeface, Typeface.BOLD)
            } else {
                messageTV!!.setTypeface(messageTV!!.typeface, Typeface.NORMAL)
            }
        }

        fun setName(username: String) {
            name!!.text = username
        }

        fun setImage(imageLink: String) {
            if (imageLink != "default")
                Picasso.get().load(imageLink).placeholder(R.drawable.default_avatar).into(image!!)
            else
                Picasso.get().load(R.drawable.default_avatar).into(image!!)

        }
    }

    companion object {
        fun newInstance(): GroupsFragment {
            return GroupsFragment()
        }
    }


}
