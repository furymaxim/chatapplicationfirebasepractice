package com.furymaxim.chatapp.models;

public class Friends {

    private Long date;

    public Friends(){}

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Friends(Long date){
        this.date = date;
    }

}
