package com.furymaxim.chatapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {

    private String name, status, image;


    public User(){}


    public User(String name, String status, String image) {
        this.name = name;
        this.status = status;
        this.image = image;
    }

    private User(Parcel in){
        name = in.readString();
        status = in.readString();
        image = in.readString();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(status);
        dest.writeString(image);
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
