package com.furymaxim.chatapp.models;

public class Request {

    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Request(String userName) {
        this.userName = userName;
    }

    public Request() { }
}
