package com.furymaxim.chatapp.models;

public class GroupConv {

    public boolean seen;
    public long timestamp;

    public GroupConv(){

    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }


    public GroupConv(boolean seen) {
        this.seen = seen;
    }
}
