package com.furymaxim.chatapp

import android.app.Application
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class ChatApp : Application() {

    override fun onCreate() {
        super.onCreate()

        FirebaseApp.initializeApp(applicationContext)
        FirebaseDatabase.getInstance().setPersistenceEnabled(true)

        if (FirebaseAuth.getInstance().currentUser != null) {
            val mUserDatabaseRef =
                FirebaseDatabase.getInstance().reference.child("Users")
                    .child(FirebaseAuth.getInstance().currentUser!!.uid)

            mUserDatabaseRef.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(p0: DataSnapshot) {
                    mUserDatabaseRef.child("online").onDisconnect().setValue(ServerValue.TIMESTAMP)
                }
            })
        }


    }


}