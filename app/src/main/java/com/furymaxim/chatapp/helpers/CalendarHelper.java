package com.furymaxim.chatapp.helpers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CalendarHelper {
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;

    private static String dateFormat = "HH:mm";
    private static String dateFormatG = "dd/MM/yyyy";
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
    private static SimpleDateFormat sdf = new SimpleDateFormat(dateFormatG, Locale.ENGLISH);

    public static String convertMilliSecondsToHourMins(String milliSeconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(milliSeconds));

        long now = System.currentTimeMillis();
        long millies = Long.valueOf(milliSeconds);

        final long diff = now - millies;

        if (diff < 24 * HOUR_MILLIS) {
            return simpleDateFormat.format(calendar.getTime());
        } else if (diff < 48 * HOUR_MILLIS) {
            return " вчера";
        } else {
            return sdf.format(calendar.getTime());
        }
    }

    public static String convertMilliSecondsToMonth(String milliSeconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(milliSeconds));
        return sdf.format(calendar.getTime());
    }
}
