package com.furymaxim.chatapp.helpers

import android.graphics.Bitmap
import com.squareup.picasso.Transformation
import kotlin.math.min

class PicassoTransform : Transformation {

    override fun transform(source: Bitmap): Bitmap {
        val size = min(source.width, source.height)
        val x = (source.width - size) / 3
        val y = (source.height - size) / 3
        val result = Bitmap.createBitmap(source, x, y, size, size)
        if (result != source) {
            source.recycle()
        }
        return result
    }

    override fun key(): String {
        return "square()"
    }
}