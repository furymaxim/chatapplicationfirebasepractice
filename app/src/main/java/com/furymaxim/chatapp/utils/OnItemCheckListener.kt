package com.furymaxim.chatapp.utils

import com.furymaxim.chatapp.models.User

interface OnItemCheckListener {
    fun onItemCheck(item: User)
    fun onItemUncheck(item: User)
}